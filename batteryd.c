/*
 * Copyright (C) 2021 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * INCLUDES:
 *
 * - libnotify/notify.h: all the libnotify functions and types
 * - stdio.h: printf(), fputs(), sprintf(), fgets()
 * - stdlib.h: calloc(), free(), atoi()
 * - stdbool.h: bool type
 * - signal.h: signal()
 * - string.h: strsignal(), strlen()
 * - unistd.h: sleep()
 */

#include <libnotify/notify.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>

struct alert_s
{
	int capacity;
	NotifyUrgency urgency;
	bool displayed;
};

#define ARRAY_LEN(array) (sizeof(array) / sizeof(array[0]))

/* ************************* CONFIGURATION ****************************** */

/* Seconds to wait between checks */
#define DELAY 60

/* Name of the battery in /sys/class/power_supply */
#define BATTERY "BAT0"

/* Icon of the notification. Can be set to NULL */
#define ICON "/usr/share/icons/Adwaita/32x32/legacy/battery-low.png"

/*
 * This array sets the capacities at which the daemon shall display a
 * notification, and the urgency of the notifications. The third field is for
 * internal use and must be initialized to false. NOTE: the order MUST be from
 * bigger to smaller capacities.
 */
static struct alert_s alerts[] = {
	{20, NOTIFY_URGENCY_NORMAL, false},
	{10, NOTIFY_URGENCY_CRITICAL, false},
	{5, NOTIFY_URGENCY_CRITICAL, false},
};

/* ********************************************************************** */

/* Global variables */
static bool keep_alive;

/*
 * This function is the handler for the terminating signals. It displays a
 * message acknowledging the signal reception and unsets the flag that keeps
 * the daemon running.
 */
static void
exit_handler(int sig)
{
	printf("Received %s. Exiting...\n", strsignal(sig));
	keep_alive = false;
}

/*
 * Returns true if BATTERY is discharging (what a surprise, huh?)
 */
static bool
is_discharging()
{
	char *state_path;
	FILE *f;
	bool discharging;

	if (!(state_path = calloc(32 + strlen(BATTERY), sizeof(char))))
	{
		perror("calloc()");
		return (false);
	}
	sprintf(state_path, "/sys/class/power_supply/%s/status", BATTERY);

	if (!(f = fopen(state_path, "r")))
	{
		perror(state_path);
		free(state_path);
		return (false);
	}
	discharging = fgetc(f) == 'D';

	fclose(f);
	free(state_path);
	return (discharging);
}

/*
 * Returns the current capacity of the battery. Returns -1 in case of error.
 */
static int
get_capacity()
{
	char *capacity_path;
	FILE *f;
	char buffer[4];

	if (!(capacity_path = calloc(34 + strlen(BATTERY), sizeof(char))))
	{
		perror("calloc()");
		return (-1);
	}
	sprintf(capacity_path, "/sys/class/power_supply/%s/capacity", BATTERY);

	if (!(f = fopen(capacity_path, "r")))
	{
		perror(capacity_path);
		free(capacity_path);
		return (-1);
	}
	fgets(buffer, 4, f);

	fclose(f);
	free(capacity_path);
	return (atoi(buffer));
}

/*
 * This functions allocates/updates a notification with the alert data and sets
 * it to displayed.
 */
static void
display_notification(NotifyNotification **notification, struct alert_s *alert)
{
	char body[15];

	sprintf(body, "%d%% remaining", alert->capacity);

	if (*notification)
		notify_notification_update(*notification, "Battery Alert", body, ICON);
	else
		*notification = notify_notification_new("Battery Alert", body, ICON);
	notify_notification_set_urgency(*notification, alert->urgency);
	notify_notification_show(*notification, NULL);

	alert->displayed = true;
}

/*
 * Mainloop of the daemon, it will keep checking the capacity and state of
 * the battery as long as the global keep_alive is true.
 */
static bool
batteryd_daemon()
{
	NotifyNotification *notification = NULL;
	int capacity;

	keep_alive = true;
	while (keep_alive)
	{
		if ((capacity = get_capacity()) == -1)
		{
			if (notification)
				g_object_unref(G_OBJECT(notification));
			return (false);
		}

		if (is_discharging())
		{
			/* Show the alert if it hasn't been displayed and is due time */
			for (size_t i = 0; i < ARRAY_LEN(alerts); i++)
			{
				if (!alerts[i].displayed && capacity <= alerts[i].capacity)
					display_notification(&notification, &alerts[i]);
			}
		}
		else
		{
			/* Unset displayed flag if the current capacity is bigger again */
			for (size_t i = 0; i < ARRAY_LEN(alerts); i++)
			{
				if (alerts[i].displayed && capacity > alerts[i].capacity)
					alerts[i].displayed = false;
			}
		}
		sleep(DELAY);
	}

	if (notification)
		g_object_unref(G_OBJECT(notification));
	return (true);
}

int 
main()
{
	int exit_status;

	if (!notify_init("Batteryd"))
	{
		fputs("Can't initialize libnotify\n", stderr);
		return (1);
	}

	signal(SIGTERM, exit_handler);
	signal(SIGINT, exit_handler);
	exit_status = batteryd_daemon() ? 0 : 1;

	notify_uninit();
	return (exit_status);
}
