# BatteryD

BatteryD is a simple daemon to display notifications when running low on battery.

#### Configuration

The program is configured in the section `CONFIGURATION` inside the source code. It has three fields:

- *DELAY*: how many seconds to wait between each battery check.
- *BATTERY*: the name of the battery in `/sys/class/power_supply/`.
- *ICON*: the path to the notification's icon. Can be set to `NULL`.
- *alerts[]*: this is an array with the capacities of the battery at which a notification shall be displayed. Each element of the array is an struct with:
   - The capacity.
   - The [urgency](https://developer.gnome.org/libnotify/0.7/NotifyNotification.html#NotifyUrgency) of the notification.
   - Must be set to `false`, used internally by the program to mark if it has already been displayed.

### Manual build

#### Dependencies
- Make
- GCC/Clang
- Pkgconf
- Libnotify

#### Build
Build with `make`. That will leave you the `batteryd` executable.

#### Installation
You can directly run the compiled executable, but if you want to install it
on your system, `make install` will do it. Notice that you can set the
variable `PREFIX` (default to /usr/local) to your desire.

#### Uninstall
`make uninstall` (Mind the `PREFIX` too)
